import React from 'react';
import Advertiser from './Advertiser.react';

export default ({products}) =>
  <div>
    { products.map((advertiser, i) => {
      return <Advertiser key={i} advertiser={advertiser} />
    })}
  </div>
