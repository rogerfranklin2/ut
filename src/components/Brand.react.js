import React from 'react';
import Product from './Product.react';

export default ({brand}) =>
  <div className="brand">
    ->&nbsp;{ brand.name }
    { brand.products.map((product, i) => {
      return <Product key={i} product={product} />
    })}
  </div>
