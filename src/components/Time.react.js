import React from 'react';
import moment from 'moment';

export default ({time}) =>
 <div className="time">
  ->&nbsp;{moment(time).format("DD/MM/Y h:mm:ss")}
 </div>
