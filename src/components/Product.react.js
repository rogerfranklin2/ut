import React from 'react';
import Time from './Time.react';

export default ({product}) =>
 <div className="product">
  ->&nbsp;{product.name}
  { product.times.map((time, i) => {
    return <Time key={i} time={time} />
  })}
 </div>
