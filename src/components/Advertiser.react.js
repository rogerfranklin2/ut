import React from 'react';
import Brand from './Brand.react';

export default ({advertiser}) =>
  <div>
    {advertiser.name}
    {advertiser.brands.map((brand, i) => {
        return <Brand key={i} brand={brand} />
    })}
  </div>
