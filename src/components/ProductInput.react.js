import React from 'react';

class ProductInput extends React.Component {
  constructor() {
    super();
    this.state = {
      advertiser: '',
      brand: '',
      product: '',
      error: false
    };

    this.addProduct = this.addProduct.bind(this);
    this.advertiserChange = this.advertiserChange.bind(this);
    this.productChange = this.productChange.bind(this);
    this.brandChange = this.brandChange.bind(this);
  }

  addProduct() {
    if(this.inputsFilled()) {
      this.props.addProduct(this.state.advertiser, this.state.brand, this.state.product);
    } else {
      this.setState({error: true});
    }
  }

  inputsFilled() {
    const { advertiser, brand, product} = this.state;

    return advertiser.trim() != '' && brand.trim() != '' && product.trim() != ''
  }

  advertiserChange(e) {
    this.setState({advertiser: e.target.value, error: false});
  }

  brandChange(e) {
    this.setState({brand: e.target.value, error: false});
  }

  productChange(e) {
    this.setState({product: e.target.value, error: false});
  }

  render() {
    const { error, advertiser, brand, product } = this.state;

    return(
      <div>
        { this.state.error ? <div>Inputs cannot be blank</div> : null }
        <label>
          Advertiser
          <input type="text"
            value={advertiser}
            onChange={this.advertiserChange}/>
        </label>
        <label>
          Brand
          <input type="text"
            value={brand}
            onChange={this.brandChange}/>
        </label>
        <label>
          Product
          <input type="text"
            value={product}
            onChange={this.productChange}/>
        </label>
        <button onClick={this.addProduct}>Add</button>
      </div>
    );
  }
}

export default ProductInput
