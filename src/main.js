import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import MainPage from './containers/MainPage.react';

import productReducer from './reducers/productReducer';

import './sass/main.sass'

const store = createStore(productReducer);

ReactDOM.render(
  <Provider store={store}>
    <MainPage />
  </Provider>,
  document.getElementById('app')
);
