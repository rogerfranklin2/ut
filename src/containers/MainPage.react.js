import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions/actions';

import ProductInput from '../components/ProductInput.react';
import Products from '../components/Products.react';


export class MainPage extends React.Component {
  constructor() {
    super();

    this.addProduct = this.addProduct.bind(this);
  }

  addProduct(advertiser, brand, product) {
    this.props.actions.addProduct(advertiser, brand, product, new Date());
  }

  render() {
    const { products } = this.props;

    return (
      <div>
        <ProductInput addProduct={this.addProduct}/>
        <Products products={products}/>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(actions, dispatch) }
}

export default connect((state) => {return state}, mapDispatchToProps)(MainPage);
