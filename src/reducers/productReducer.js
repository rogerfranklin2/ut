import Immutable, { Map } from 'immutable';

const initialState = { products: []};

function createNewAdvertiser(action) {
  const brand = createNewBrand(action);
  const advertiser = { name: action.advertiser, brands: [brand] }

  return advertiser;
}

function createNewBrand(action) {
  const product = createNewProduct(action);
  const brand = { name: action.brand, products:[product] }

  return brand;
}

function createNewProduct(action) {
  const product = { name: action.product, times: [action.time] }

  return product;
}

function productReducer(state = initialState, action){

  switch(action.type) {
    case 'ADD_PRODUCT':
      const newState = Immutable.fromJS(state);
      const products = newState.get('products');

      const advertiserIndex = products.findIndex((product) =>  product.get('name') == action.advertiser );
      if (advertiserIndex != -1) {
        const brandIndex = products.getIn([advertiserIndex,'brands']).findIndex(brand => brand.get('name') == action.brand);
        if (brandIndex != -1) {
          const productIndex = newState.getIn(['products',advertiserIndex,'brands',brandIndex,'products']).findIndex(product => product.get('name') == action.product);
          if (productIndex != -1) {
            const timeAdded = newState.updateIn(['products', advertiserIndex, 'brands', brandIndex, 'products', productIndex , 'times'], list => list.push(action.time));

            return timeAdded.toJS();
          } else {
            const newProduct = createNewProduct(action);
            const mergedProducts = newState.updateIn(['products',advertiserIndex,'brands', brandIndex, 'products'], list => list.push(Immutable.fromJS(newProduct)));

            return mergedProducts.toJS();
          }

        } else {
          const newBrand = createNewBrand(action);
          const mergedBrands = newState.updateIn(['products',advertiserIndex,'brands'], list => list.push(Immutable.fromJS(newBrand)))

          return mergedBrands.toJS();
        }

      } else {
        const newAdvertiser = createNewAdvertiser(action);
        const mergedAdvertisers = newState.updateIn(['products'], list => list.push(Immutable.fromJS(newAdvertiser)));
        return mergedAdvertisers.toJS();
      }
      break;

    default:
      return state;
  }

}

export default productReducer;
