export function addProduct(advertiser, brand, product, time){
  return {
    type: 'ADD_PRODUCT',
    advertiser,
    brand,
    product,
    time
  };
}
