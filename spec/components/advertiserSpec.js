import React from 'react';
import { shallow } from 'enzyme';
import Advertiser from '../../src/components/Advertiser.react';
import Brand from '../../src/components/Brand.react';

describe('Advertiser', () => {
  it('should show advertiser name', () => {
    const component = shallow(<Advertiser advertiser={{ name: "Unilever", brands: [0,1]}} />);

    expect(component.text()).toContain("Unilever");
  });

  it('should show all brands for advertiser', () => {
    const component = shallow(<Advertiser advertiser={{ name: "Unilever", brands: [0,1]}} />);

    expect(component.find(Brand).length).toBe(2);
  });
});
