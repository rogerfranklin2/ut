import React from 'react';
import { shallow } from 'enzyme';
import Products from '../../src/components/Products.react';
import Advertiser from '../../src/components/Advertiser.react';

describe('Products', () => {
  it('should not render any advertisers', () => {
    const component = shallow(<Products products={[]}/>);

    expect(component.find(Advertiser).length).toBe(0);
  });

  it('should render an advertiser for each advertsier', () => {
    const component = shallow(<Products products={[0,1]} />);

    expect(component.find(Advertiser).length).toBe(2);
  });
});
