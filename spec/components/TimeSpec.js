import React from 'react';
import { shallow } from 'enzyme';
import Time from '../../src/components/Time.react';

describe('Time', () => {
  it('show the time formatted', () => {
    const component = shallow(<Time time={new Date('2016','03','12','10','15','23')}/>);

    expect(component.text()).toContain("12/04/2016 10:15:23");
  });
});
