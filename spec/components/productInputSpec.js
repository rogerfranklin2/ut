import React from 'react';
import { shallow } from 'enzyme';
import ProductInput from '../../src/components/ProductInput.react';
import sinon from 'sinon';

describe('productInput', () => {
  let component;
  let labels;
  let button;
  beforeAll(() => {
    component = shallow(<ProductInput />);
    labels = component.find('label');
    button = component.find('button');
  });

  it('should render 3 inputs', () => {
    expect(component.find('input').length).toBe(3);
  });

  it('should have a label for  each input', () => {

    expect(labels.at(0).text()).toContain("Advertiser");
    expect(labels.at(1).text()).toContain("Brand");
    expect(labels.at(2).text()).toContain("Product");
  });

  describe('input properties', () => {
    it('should set advertiser input value to be advertiser state', () => {

    })
  });

  it('should have a button named Add', () => {
    expect(component.find('button').text()).toBe("Add");
  });

  describe('input validation', () => {
    let spy;
    let sinonComponent;

    beforeEach(() => {
      spy = sinon.spy();
      sinonComponent = shallow(<ProductInput addProduct={spy} />);
    });

    it('should not call if advertiser is blank', () => {
      sinonComponent.setState({advertiser: '', brand: 'b', product: 'c'});
      sinonComponent.find('button').simulate('click');
      expect(spy.callCount).toBe(0);
      expect(sinonComponent.state('error')).toBe(true);
    });

    it('should not call if brand is blank', () => {
      sinonComponent.setState({advertiser: 'a', brand: '', product: 'c'});
      sinonComponent.find('button').simulate('click');
      expect(spy.callCount).toBe(0);
      expect(sinonComponent.state('error')).toBe(true);
    });

    it('should not call if product is blank', () => {
      sinonComponent.setState({advertiser: 'a', brand: 'b', product: ''});
      sinonComponent.find('button').simulate('click');
      expect(spy.callCount).toBe(0);
      expect(sinonComponent.state('error')).toBe(true);
    });

    it('should call if all inputs filled', () => {
      sinonComponent.setState({advertiser: 'a', brand: 'b', product: 'c'});
      sinonComponent.find('button').simulate('click');
      expect(spy.callCount).toBe(1);
      expect(sinonComponent.state('error')).toBe(false);
    });

    it('should call with state of inputs', () => {
     sinonComponent.setState({advertiser: 'a', brand: 'b', product: 'c'});
     sinonComponent.find('button').simulate('click');
     expect(spy.args[0][0]).toBe('a');
     expect(spy.args[0][1]).toBe('b');
     expect(spy.args[0][2]).toBe('c');
   });
 });

 it('should show error message when error', () => {
   component.setState({error: true});
   component.update();

   expect(component.text()).toContain("Inputs cannot be blank");
 });

 it('should hide message when error is false', () => {
   component.setState({error: false});
   component.update();

   expect(component.text()).not.toContain("Inputs cannot be blank");
 });

});
