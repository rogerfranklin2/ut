import React from 'react';
import { shallow } from 'enzyme';
import { MainPage } from '../../src/containers/MainPage.react';
import ProductInput from '../../src/components/ProductInput.react';
import Products from '../../src/components/Products.react';
import sinon from 'sinon';

describe('MainPage', () => {
  let component;
  beforeAll(() => {
    jasmine.clock().install();
    jasmine.clock().mockDate(new Date('2016','05','01'));
    component = shallow(<MainPage />);
  });

  afterAll(() => {
    jasmine.clock().uninstall();
  });

  it('should render a product input', () => {
    expect(component.find(ProductInput).length).toBe(1);
  });

  it('should render a list of products', () => {
    expect(component.find(Products).length).toBe(1);
  });

  it('should call action when addProduct called including the time ', () => {

    const spy = sinon.spy();
    const actions = { addProduct: spy };
    const sinonComponent = shallow(<MainPage actions={actions} />);

    sinonComponent.instance().addProduct('x','y','z');
    expect(spy.args[0][0]).toBe('x');
    expect(spy.args[0][1]).toBe('y');
    expect(spy.args[0][2]).toBe('z');
    expect(spy.args[0][3].toString()).toBe('Wed Jun 01 2016 00:00:00 GMT+0100 (BST)');
  });
});
