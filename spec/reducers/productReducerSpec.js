import reducer from '../../src/reducers/ProductReducer';

describe('productReducer', () => {
  it('should render default state', () => {
    expect(reducer(undefined, {})).toEqual({ products: []});
  });

  it('should set product', () => {
    const advertiser = 'big advertiser';
    const brand = 'shiny brand';
    const product = 'best product';
    const time = 'now';
    const action = { type: 'ADD_PRODUCT', advertiser, brand, product, time};
    expect(reducer(undefined, action)).toEqual(
      { products: [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now']}]}] }]}
    );
  });

  it('should add addtional timestamp to existing product when added again', () => {
    const state = { products: [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now']}]}] }]}
    const advertiser = 'big advertiser';
    const brand = 'shiny brand';
    const product = 'best product';
    const time = 'later';

    const action = { type: 'ADD_PRODUCT', advertiser, brand, product, time};
    expect(reducer(state, action)).toEqual(
      { products: [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now', 'later']}]}] }]}
    );
  });

  it('should add aditional additional advertiser, brand and product', () => {
    const state = { products: [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now']}]}] }]}
    const advertiser = 'Bigger advertiser';
    const brand = 'shinier brand';
    const product = 'better product';
    const time = 'now';

    const action = { type: 'ADD_PRODUCT', advertiser, brand, product, time};
    expect(reducer(state, action)).toEqual(
      { products:
        [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now']}]}] },
         {name: 'Bigger advertiser', brands: [ {name: 'shinier brand', products: [ {name : 'better product', times: ['now']}]}]}
        ]
      }
    );
  });

  it('should add addtional brand if brand does not exist', () => {
    const state = { products: [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now']}]}] }]}
    const advertiser = 'big advertiser';
    const brand = 'shinier brand';
    const product = 'better product';
    const time = 'now';

    const action = { type: 'ADD_PRODUCT', advertiser, brand, product, time};
    expect(reducer(state, action)).toEqual(
      { products:
        [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now']}]},
                                            {name: 'shinier brand', products: [ {name : 'better product', times: ['now']}]}
                                          ]}]
      }
    );
  });

  it('should add addtional product if brand exsits but product does not ', () => {
    const state = { products: [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now']}]}] }]}
    const advertiser = 'big advertiser';
    const brand = 'shiny brand';
    const product = 'better product';
    const time = 'now';

    const action = { type: 'ADD_PRODUCT', advertiser, brand, product, time};
    expect(reducer(state, action)).toEqual(
      { products:
        [{name: 'big advertiser', brands: [ {name: 'shiny brand', products: [ {name : 'best product', times: ['now']},{name : 'better product', times: ['now']}]}
                                          ]}]
      }
    );
  });
});
