import * as actions from '../../src/actions/actions';

describe('actions', () => {
  it('should add product action', () => {
    const advertiser = 'big advertiser';
    const brand = 'shiny brand';
    const product = 'best product';
    const time = 'now';
    const expectedAction = {
      type: 'ADD_PRODUCT',
      advertiser,
      brand,
      product,
      time
    };
    expect(actions.addProduct(advertiser, brand, product, time)).toEqual(expectedAction);
  });
});
